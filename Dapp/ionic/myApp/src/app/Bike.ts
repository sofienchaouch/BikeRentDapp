export class Bike {

    public id : number;
    public location : string ;
    public for_rent : boolean;
    public price : number;
    public bike_type : string;
    public owner : string;
    public locked : boolean;
    //rent mesurments
    public renter : string;
    public max_time : number ;
    public begin_rent_time : number ;
    public payed : number;
    public key : number 
    
    constructer(id: number,location: string ,for_rent: boolean,price: number,bike_type: string,owner: string,locked:boolean,renter:string,max_time : number,begin_rent_time: number,payed:number,key:number){
        this.id = id;
        this.location = location ;
        this.for_rent = for_rent;
        this.price = price;
        this.bike_type = bike_type;
        this.owner = owner;
        this.locked = locked;
    //rent mesurments
        this.renter = renter;
        this.max_time = max_time ;
        this.begin_rent_time = begin_rent_time ;
        this.payed = payed;
        this.key = key;
    }
}