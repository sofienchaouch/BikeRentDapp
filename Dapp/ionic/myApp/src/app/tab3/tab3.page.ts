import { Component,HostListener, NgZone } from '@angular/core';
import { Bike } from '../Bike';
import {User} from '../User';
import {UserServiceService,Web3Service} from '../services/services';

declare var window: any;

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  account = "0x0e2D6AeC0D71c79C09c9E05dfdf73Ed65B0c7BCf";
  accounts: any;
  user : User;
  balance: number;
  id : number ;
  name : string ;
  cin : string ;
  mail : string;
  phone_number : string;
  sendingAmount: number;
  recipientAddress: string;
  status: string;
 // canBeNumber = canBeNumber;

  public _ngZone: NgZone;
  public web3Service: Web3Service;
  public userServiceService: UserServiceService; 
// tslint:disable-next-line: use-life-cycle-interface
  ngOnInit() {
    this.account = "0x0e2D6AeC0D71c79C09c9E05dfdf73Ed65B0c7BCf";
    this.balance = 1;
    this.id = 1 ;
    this.cin = "0010120101";
    this.onReady();

  }

  constructor() {
  }

  onReady = () => {

    // Get the initial account balance so it can be displayed.
    this.web3Service.getAccounts().subscribe(accs => {
    this.accounts = accs;
    this.account = this.accounts[0];

      // This is run from window:load and ZoneJS is not aware of it we
      // need to use _ngZone.run() so that the UI updates on promise resolution
    this._ngZone.run(() =>
      this.refreshBalance(),
      this.getProfile()
    );
    }, err => alert(err))
  }

  refreshBalance = () => {
    this.userServiceService.getBalance(this.account)
      .subscribe(value => {
        this.balance = value
      }, e => {this.setStatus('Error getting balance; see log.')})
  };

  setStatus = message => {
    this.status = message;
  };

  getProfile = () => {
    this.userServiceService.getUser(this.account)
      .subscribe(value => {
        this.id = value[1];
        this.name = value[2];
        this.cin = value[3];
        this.mail = value[4] ;
        this.phone_number = value[5];
      }, e => {this.setStatus('Error getting balance; see log.')})
  };

  editProfile = () => {
    this.setStatus('Initiating transaction... (please wait)');

    this.userServiceService.EditProfile(this.mail, this.phone_number ,this.account)
      .subscribe(() =>{
        this.setStatus('Transaction complete!');
        this.refreshBalance();
      }, e => this.setStatus('Error sending coin; see log.'))
  };

}
