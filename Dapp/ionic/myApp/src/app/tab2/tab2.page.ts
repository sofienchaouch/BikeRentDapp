import { Component, HostListener, NgZone, OnInit } from '@angular/core';
import { Bike } from '../Bike';
import {User} from '../User';
import {UserServiceService,Web3Service} from '../services/services';
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})

export class Tab2Page implements OnInit{

  Personal_Bike: Bike ;
  Rented_Bike: Bike;


  private web3Service: Web3Service;
  private userServiceService: UserServiceService;

  account: any;
  accounts: any;
  user: User;
  balance: number;
  id: number;
  name: string;
  cin: string;
  mail: string;
  phone_number: string;
  sendingAmount: number;
  recipientAddress: string;
  status: string;
  position: string;
  for_rent: boolean;
  price: number;
  bike_type: string;
  from : any;

  constructor(private _ngZone: NgZone){

  }

  ngOnInit() {

  }

  addBike = () => {
    this.setStatus('Initiating transaction... (please wait)');

    this.userServiceService.add_Bike(this.id, this.position, this.for_rent, this.price, this.bike_type, this.from)
      .subscribe(() => {
        this.setStatus('Transaction complete!');
        this.refreshBalance();
      }, e => this.setStatus('Error sending coin; see log.'))
  }

  //TODO
  getBike =  () => {
    this.userServiceService.getBike(this.id, this.account)
    .subscribe(value => {
      //this.Personal_Bike = new Bike(value[1],this.account); 
      }, e => { this.setStatus('Error getting balance; see log.') })
  };

  DeleteBike = () => {
    this.setStatus('Initiating transaction... (please wait)');

    this.userServiceService.Delete_Bike(this.Personal_Bike.id, this.account)
      .subscribe(() => {
        this.setStatus('Transaction complete!');
        this.refreshBalance();
      }, e => this.setStatus('Error sending coin; see log.'))
  }

  userpostBikeFoRent = () => {
    this.setStatus('Initiating transaction... (please wait)');

    this.userServiceService.userpost_bike_for_rent(this.account)
      .subscribe(() => {
        this.setStatus('Transaction complete!');
        this.refreshBalance();
      }, e => this.setStatus('Error sending coin; see log.'))
  }


  userRemoveBikeFromRent = () => {
    this.setStatus('Initiating transaction... (please wait)');

    this.userServiceService.user_remove_bike_from_rent(this.account)
      .subscribe(() => {
        this.setStatus('Transaction complete!');
        this.refreshBalance();
      }, e => this.setStatus('Error sending coin; see log.'))
  }


  stopRent = () => {
    this.setStatus('Initiating transaction... (please wait)');

    this.userServiceService.stop_rent(this.position ,this.account)
      .subscribe(() => {
        this.setStatus('Transaction complete!');
        this.refreshBalance();
      }, e => this.setStatus('Error sending coin; see log.'))
  }


  openLock = () => {
    this.setStatus('Initiating transaction... (please wait)');

    this.userServiceService.openLock(this.id, this.account)
      .subscribe(() => {
        this.setStatus('Transaction complete!');
        this.refreshBalance();
      }, e => this.setStatus('Error sending coin; see log.'))
  }


  closeLock = () => {
    this.setStatus('Initiating transaction... (please wait)');

    this.userServiceService.closeLock(this.id, this.account)
      .subscribe(() => {
        this.setStatus('Transaction complete!');
        this.refreshBalance();
      }, e => this.setStatus('Error sending coin; see log.'))

  } 
  onReady = () => {

    // Get the initial account balance so it can be displayed.
    this.web3Service.getAccounts().subscribe(accs => {
      this.accounts = accs;
      this.account = this.accounts[0];

      // This is run from window:load and ZoneJS is not aware of it we
      // need to use _ngZone.run() so that the UI updates on promise resolution
      this._ngZone.run(() =>
        this.refreshBalance());
    }, err => alert(err))
  }

  refreshBalance = () => {
    this.userServiceService.getBalance(this.account)
      .subscribe(value => {
        this.balance = value
      }, e => { this.setStatus('Error getting balance; see log.') })
  };

  setStatus = message => {
    this.status = message;
  };

}
