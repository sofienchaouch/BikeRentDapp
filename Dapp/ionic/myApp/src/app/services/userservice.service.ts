/*import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor() { }
}
*/
import { Injectable } from '@angular/core';
//import { from } from 'rxjs';
import { Observable } from 'rxjs'
//import { fromPromise } from 'rxjs/observable/fromPromise';
import { Web3Service } from './web3.service';
//var observableFromPromise =  from(promiseSrc);
var require: any
const BikeRentArtifacts = require('../../../build/contracts/BikeRent.json');
const contract = require('truffle-contract');

@Injectable({
	providedIn: 'root'
})
export class UserServiceService {

	BikeRent = contract(BikeRentArtifacts);

  constructor(
  	public web3Ser: Web3Service,
  	) { 
  	// Bootstrap the MetaCoin abstraction for Use
  	this.BikeRent.setProvider(web3Ser.web3.currentProvider);
  }

  getBalance(account): Observable<number> {
  	let meta;

  	return Observable.create(observer => {
  		this.BikeRent
  		  .deployed()
  		  .then(instance => {
  		    meta = instance;
          //we use call here so the call doesn't try and write, making it free
  		    return meta.getBalance.call(account, {
  		      from: account
  		    });
  		  })
  		  .then(value => {
  		    observer.next(value)
  		    observer.complete()
  		  })
  		  .catch(e => {
  		    console.log(e);
  		    observer.error(e)
  		  });
  	})
  }

  getUser(account): Observable<number> {
  	let meta;

  	return Observable.create(observer => {
  		this.BikeRent
  		  .deployed()
  		  .then(instance => {
  		    meta = instance;
          //we use call here so the call doesn't try and write, making it free
  		    return meta.getUser.call(account, {
  		      from: account
  		    });
  		  })
  		  .then(value => {
  		    observer.next(value)
  		    observer.complete()
  		  })
  		  .catch(e => {
  		    console.log(e);
  		    observer.error(e)
  		  });
  	})
  }

	getBike(id, account): Observable<number> {
  	let meta;

  	return Observable.create(observer => {
  		this.BikeRent
  		  .deployed()
  		  .then(instance => {
  		    meta = instance;
          //we use call here so the call doesn't try and write, making it free
  		    return meta.getBike.call(id, {
  		      from: account
  		    });
  		  })
  		  .then(value => {
  		    observer.next(value)
  		    observer.complete()
  		  })
  		  .catch(e => {
  		    console.log(e);
  		    observer.error(e)
  		  });
  	})
  }

  sendCoin(from, to, amount): Observable<any>{

  	let meta;
  	return Observable.create(observer => {
  	  this.BikeRent
  	    .deployed()
  	    .then(instance => {
  	      meta = instance;
  	      return meta.sendCoin(to, amount, {
  	        from: from
  	      });
  	    })
  	    .then(() => {
  	      observer.next()
  	      observer.next()
  	    })
  	    .catch(e => {
  	    	console.log(e);
  	      observer.error(e)
  	    });
  	})
	}
	
	EditProfile(mail, phone,from): Observable<any>{

  	let bRent;
  	return Observable.create(observer => {
  	  this.BikeRent
  	    .deployed()
  	    .then(instance => {
  	      bRent = instance;
  	      return bRent.edit_profile(mail, phone, {
  	        from: from
  	      });
  	    })
  	    .then(() => {
  	      observer.next()
  	      observer.next()
  	    })
  	    .catch(e => {
  	    	console.log(e);
  	      observer.error(e)
  	    });
  	})
	}

	Delete_Bike(id, from): Observable<any>{

  	let bRent;
  	return Observable.create(observer => {
  	  this.BikeRent
  	    .deployed()
  	    .then(instance => {
  	      bRent = instance;
  	      return bRent.delete_Bike(id, {
  	        from: from
  	      });
  	    })
  	    .then(() => {
  	      observer.next()
  	      observer.next()
  	    })
  	    .catch(e => {
  	    	console.log(e);
  	      observer.error(e)
  	    });
  	})
  }

	userpost_bike_for_rent(from): Observable<any>{

  	let bRent;
  	return Observable.create(observer => {
  	  this.BikeRent
  	    .deployed()
  	    .then(instance => {
  	      bRent = instance;
  	      return bRent.user_post_bike_for_rent( {
  	        from: from
  	      });
  	    })
  	    .then(() => {
  	      observer.next()
  	      observer.next()
  	    })
  	    .catch(e => {
  	    	console.log(e);
  	      observer.error(e)
  	    });
  	})
  }
  
  user_remove_bike_from_rent(from): Observable<any>{

  	let bRent;
  	return Observable.create(observer => {
  	  this.BikeRent
  	    .deployed()
  	    .then(instance => {
  	      bRent = instance;
  	      return bRent.user_remove_From_rent( {
  	        from: from
  	      });
  	    })
  	    .then(() => {
  	      observer.next()
  	      observer.next()
  	    })
  	    .catch(e => {
  	    	console.log(e);
  	      observer.error(e)
  	    });
  	})
  }

  Rent(id , hours , from): Observable<any>{

  	let bRent;
  	return Observable.create(observer => {
  	  this.BikeRent
  	    .deployed()
  	    .then(instance => {
  	      bRent = instance;
  	      return bRent.Rent_Bike(id , hours, {
  	        from: from
  	      });
  	    })
  	    .then(() => {
  	      observer.next()
  	      observer.next()
  	    })
  	    .catch(e => {
  	    	console.log(e);
  	      observer.error(e)
  	    });
  	})
  }


  stop_rent(location, from): Observable<any>{

  	let bRent;
  	return Observable.create(observer => {
  	  this.BikeRent
  	    .deployed()
  	    .then(instance => {
  	      bRent = instance;
  	      return bRent.stop_rent(location , {
  	        from: from
  	      });
  	    })
  	    .then(() => {
  	      observer.next()
  	      observer.next()
  	    })
  	    .catch(e => {
  	    	console.log(e);
  	      observer.error(e)
  	    });
  	})
  }


  openLock(id, from): Observable<any>{

  	let bRent;
  	return Observable.create(observer => {
  	  this.BikeRent
  	    .deployed()
  	    .then(instance => {
  	      bRent = instance;
  	      return bRent.openLock(id, {
  	        from: from
  	      });
  	    })
  	    .then(() => {
  	      observer.next()
  	      observer.next()
  	    })
  	    .catch(e => {
  	    	console.log(e);
  	      observer.error(e)
  	    });
  	})
  }


  closeLock(id, from): Observable<any>{

    let bRent;
  	return Observable.create(observer => {
  	  this.BikeRent
  	    .deployed()
  	    .then(instance => {
  	      bRent = instance;
  	      return bRent.closeLock(id, {
  	        from: from
  	      });
  	    })
  	    .then(() => {
  	      observer.next()
  	      observer.next()
  	    })
  	    .catch(e => {
  	    	console.log(e);
  	      observer.error(e)
  	    });
  	})
  }

  
  add_Bike(id, position,for_rent,price,bike_type,from): Observable<any>{

    let bRent;
  	return Observable.create(observer => {
  	  this.BikeRent
  	    .deployed()
  	    .then(instance => {
  	      bRent = instance;
  	      return bRent.add_Bike(id,position,for_rent,price,bike_type ,{
  	        from: from
  	      });
  	    })
  	    .then(() => {
  	      observer.next()
  	      observer.next()
  	    })
  	    .catch(e => {
  	    	console.log(e);
  	      observer.error(e)
  	    });
  	})
  }


	getBikesNum(account): Observable<number> {
		let meta;

		return Observable.create(observer => {
			this.BikeRent
				.deployed()
				.then(instance => {
					meta = instance;
					return meta.getBikesNum.call( {
						from: account
					});
				})
				.then(value => {
					observer.next(value)
					observer.complete()
				})
				.catch(e => {
					console.log(e);
					observer.error(e)
				});
		})
	}

	getUsersNum(id, account): Observable<number> {
		let meta;

		return Observable.create(observer => {
			this.BikeRent
				.deployed()
				.then(instance => {
					meta = instance;
					//we use call here so the call doesn't try and write, making it free
					return meta.getUsersNum.call( {
						from: account
					});
				})
				.then(value => {
					observer.next(value)
					observer.complete()
				})
				.catch(e => {
					console.log(e);
					observer.error(e)
				});
		})
	}

	getBikes(id ,account): Observable<number> {
		let meta;

		return Observable.create(observer => {
			this.BikeRent
				.deployed()
				.then(instance => {
					meta = instance;
					return meta.getBikes.call(id ,{
						from: account
					});
				})
				.then(value => {
					observer.next(value)
					observer.complete()
				})
				.catch(e => {
					console.log(e);
					observer.error(e)
				});
		})
	}

	getUsers(addr, account): Observable<number> {
		let meta;

		return Observable.create(observer => {
			this.BikeRent
				.deployed()
				.then(instance => {
					meta = instance;
					return meta.getUsersNum.call(addr,{
						from: account
					});
				})
				.then(value => {
					observer.next(value)
					observer.complete()
				})
				.catch(e => {
					console.log(e);
					observer.error(e)
				});
		})
	}

}