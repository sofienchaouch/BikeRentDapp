import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Web3Service} from './web3.service';
import { UserServiceService} from './userservice.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    Web3Service,
    UserServiceService
  ],
  declarations: []
})
export class UtilModule {
}
