import { Component ,HostListener, NgZone } from '@angular/core';
import { Bike } from '../Bike';
import {User} from '../User';
import {UserServiceService,Web3Service} from '../services/services';

declare var window: any;

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {}
