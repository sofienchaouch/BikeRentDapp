import { Component ,HostListener, NgZone } from '@angular/core';
import { Bike } from '../Bike';
import {User} from '../User';
import {UserServiceService,Web3Service} from '../services/services';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

export class Tab1Page {

  private ngZone: NgZone;
  private web3Service: Web3Service;
  private userServiceService: UserServiceService;
  private barcodeScanner: BarcodeScanner;
  qrData = null;
  createdCode = null;
  scannedCode = null;

  id: number;
  hours : number;
  from : string;
  account: any;
  accounts: any;
  balance: number;
  status: string;
  bikesForRent: Bike[];

  constructor(){
    
  }

  createCode() {
    this.createdCode = this.qrData;
  }

  scanCode() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;
    })
  }

  Rent = () => {
    this.setStatus('Initiating transaction... (please wait)');

    this.userServiceService.Rent(this.id, this.hours, this.from)
      .subscribe(() => {
        this.setStatus('Transaction complete!');
        this.refreshBalance();
      }, e => this.setStatus('Error sending coin; see log.'))
  };

  getBikesForRent = () => {
    this.userServiceService.getBikesNum(this.account)
      .subscribe(value => {
        for (var _i = 0; _i < value ; _i++){
          this.userServiceService.getBikesNum(this.account)
            .subscribe(value2 => {
              this.userServiceService.getBikes(value2 ,this.account)
                .subscribe(value3 => {
// tslint:disable-next-line: max-line-length
                 this.bikesForRent.push(new Bike())

              },)

            },)
        }
      }, e => { this.setStatus('Error getting balance; see log.') })

  };

  refreshBalance = () => {
    this.userServiceService.getBalance(this.account)
      .subscribe(value => {
        this.balance = value
      }, e => { this.setStatus('Error getting balance; see log.') })
  };



  setStatus = message => {
    this.status = message;
  };


}
